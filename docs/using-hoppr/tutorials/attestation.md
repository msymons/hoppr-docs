---
sidebar_position: 4
---

# Attestation

Hoppr uses [in-toto](https://in-toto.io) to provide traceability and attestation capability.

## in-toto Layout Overview

The first step is to define the [supply chain layout](https://github.com/in-toto/in-toto#layout), a JSON-formatted file
containing the steps that will be carried out in order to create the final product. In this case, the generated hoppr
bundle is the final product. The layout file must be created by a project owner and signed with the project owner key.

Each step in the layout defines rules for what may and may not be included in the final hoppr bundle and must be signed
by a functionary, the individual or automated process that will perform that step. An additional functionary key must
be supplied in order to sign each step in the layout. The outputs expected to be produced by each step (products) are
expected as inputs (materials) to each subsequent step. For the purposes of creating a hoppr bundle, the steps would be
defined as:

- `_collect_metadata` - Capture metadata about the conditions under which the bundle was generated. This metadata
  includes:
  - Manifest, transfer, and/or credentials files provided to hoppr
  - Metadata about the runtime environment (`generic/_metadata_/_run_data_`)
  - All input SBOMs consolidated into single SBOM file (`generic/_metadata_/_consolidated_bom.json`)
- One step for each stage defined in the hoppr transfer file (e.g. `Delta`, `Collect`, `Bundle`)
- `_finalize` - Write the final SBOM file (`generic/_metadata_/_delivered_bom.json`) containing the additional
  metadata/properties for each component added by hoppr

Once generated, [verification](https://github.com/in-toto/in-toto#verification) can be performed on the hoppr bundle.

See [this link](https://github.com/in-toto/docs/blob/v0.9/in-toto-spec.md#16-terminology) for a full list of the
terminology used by in-toto.

## Usage

### Generate project owner key

Create a key pair that will be used to sign the generated software supply chain layout.

```shell
in-toto-keygen project_owner_key
```

### Generate functionary key

Create a functionary key pair that will be used to sign the individual steps defined in the layout.

```shell
in-toto-keygen functionary_key
```

### Generate in-toto layout

Provide the project owner key, functionary key, and hoppr transfer file to the `hopctl generate layout`
subcommand in order to automatically generate an in-toto layout file (named `in-toto.layout`).

```shell
hopctl generate layout --transfer transfer.yml --project-owner-key project_owner_key --functionary-key functionary_key
```

### Create hoppr bundle

Run the `hopctl bundle` subcommand as usual, ensuring to additionally specify the `--attest` and `--functionary-key`
options.

```shell
hopctl bundle --transfer transfer.yml --log hoppr_log.txt --verbose --attest --functionary-key functionary_key manifest.yml
```

### Verify bundle against in-toto layout

```shell
in-toto-verify --layout in-toto.layout --layout-keys project_owner_key.pub --verbose
```
