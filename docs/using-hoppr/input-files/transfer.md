---
sidebar_position: 3
title: Transfer
---

## Transfer

The `Transfer` file will specify the processing steps needed to collect, augment, filter, report, and bundle components.

A **Hoppr** specific YAML file that defines how to run a series of stages and plugins used to collect, process, and bundle components.

### Default Component Coverage

There are configuration options to define how frequently you want each SBOM component to be processed using the `default_component_coverage` variable.

| Variable            | Description                                                      |
| --------- | ----------- |
| `OPTIONAL`          | Processing is optional, but can be any number of times           |
| `EXACTLY_ONCE`      | The component will be processed exactly one time                 |
| `AT_LEAST_ONCE`     | The component has to be processed at least once, but can be more |
| `NO_MORE_THAN_ONCE` | The component can be processed either once or 0 times            |

### Specifying Plugins

Users have the ability to specify plugins in the transfer file in three ways:

- Short module name: `collect_pypi_plugin`
- Plugin class name: `CollectPypiPlugin`
- Full module path: `hoppr.core_plugins.collect_pypi_plugin`

#### Example `transfer.yml`

```yaml
---
schemaVersion: v1
kind: transfer

stages:
  Collect:
    plugins:
    - name: "hoppr.core_plugins.collect_docker_plugin"
    - name: "hoppr.core_plugins.collect_git_plugin"
    - name: "hoppr.core_plugins.collect_helm_plugin"
    - name: "hoppr.core_plugins.collect_maven_plugin"
    - name: "hoppr.core_plugins.collect_pypi_plugin"
      config:
        pip_command: pip3
    - name: "hoppr.core_plugins.collect_raw_plugin"
  Bundle:
    plugins:
    - name: "hoppr.core_plugins.bundle_tar"
      config:
        tarfile_name: ~/tarfile.tar.gz

max_processes: 3
```

For more information on credential configuration options see the credentials file [documentation](../input-files/credentials.md)
For more information on manifest configuration options see the manifest file [documentation](../input-files/manifest.md)
