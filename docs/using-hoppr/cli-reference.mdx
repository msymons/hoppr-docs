---
title: CLI Reference
---

## Help

To see the available command-line options on your version of Hoppr, execute:

```bash
hopctl -h
```

## `hopctl`

`Usage: hopctl [OPTIONS] COMMAND [ARGS]...`

Collect, process, & bundle your software supply chain

### Options

| Name(s)                | Shorthand | Description                                                                      |
| ---------------------- | --------- | -------------------------------------------------------------------------------- |
| `--install-completion` |           | Install completion for the current shell.                                        |
| `--show-completion`    |           | Show completion for the current shell, to copy it or customize the installation. |
| `--help`               | `-h`      | Show this message and exit.                                                      |

### Global options

| Name(s)                    | Shorthand | Env Var            | Description                                                                      |
| -------------------------- | --------- | ------------------ | -------------------------------------------------------------------------------- |
| `--basic-term`             | `-b`      | HOPPR_BASIC_TERM   | Use simplified output for non-TTY or legacy terminal emulators. (Default: False) |
| `--strict` / `--no-strict` |           | HOPPR_STRICT_REPOS | Utilize only manifest repositories for package collection. (Default: strict)     |
| `--debug`, `--verbose`     | `-v`      |                    | Enable debug output.                                                             |

### Subcommands

| Command    | Description                                                                                   |
| ---------- | --------------------------------------------------------------------------------------------- |
| `bundle`   | Run the stages specified in the transfer config file on the content specified in the manifest |
| `generate` | Generate in-toto keys/layout or schemas for Hoppr input files                                 |
| `merge`    | Merge all properties of two or more SBOM files                                                |
| `validate` | Validate multiple manifest files for schema errors                                            |
| `version`  | Print version information for `hoppr`                                                         |

### Deprecated Subcommands

| Command            | Description                                                        |
| ------------------ | ------------------------------------------------------------------ |
| `generate-layout`  | [See hopctl generate layout subcommand](#hopctl-generate-layout)   |
| `generate-schemas` | [See hopctl generate schemas subcommand](#hopctl-generate-schemas) |

## Subcommand Reference

To see further help on sub-commands (in this example, `bundle`), execute:

```bash
hopctl bundle -h
```

### `hopctl bundle`

`Usage: hopctl bundle [OPTIONS] [MANIFEST_FILE]`

Runs the stages specified in the transfer config file on the content specified in the manifest.

##### Arguments

| Name          | Description                                   |
| ------------- | --------------------------------------------- |
| manifest_file | Path to manifest file (default: manifest.yml) |

##### Options

| Name(s)                        | Shorthand  | Env Var                          | Description                                                      |
| ------------------------------ | ---------- | -------------------------------- | ---------------------------------------------------------------- |
| `--credentials`                | `-c`       | HOPPR_CREDS_CONFIG               | Specify credentials config for services.                         |
| `--transfer`                   | `-t`       | HOPPR_TRANSFER_CONFIG            | Specify transfer config. (default: transfer.yml)                 |
| `--log`                        | `-l`       | HOPPR_LOG_FILE                   | File to which log will be written.                               |
| `--attest`                     | `-a`       | HOPPR_ATTESTATION                | Generate in-toto attestations.                                   |
| `--functionary-key`            | `-fk`      | HOPPR_FUNCTIONARY_KEY            | Path to key used to sign in-toto layout.                         |
| `--prompt`                     | `-p`       | HOPPR_PROJECT_OWNER_KEY_PROMPT   | Prompt user for project owner key`s password.                    |
| `--project-owner-key-password` | `-fk-pass` | HOPPR_PROJECT_OWNER_KEY_PASSWORD | Password for project owner key.                                  |
| `--previous-delivery`          | `-pd`      | HOPPR_PREVIOUS_DELIVERY          | Path to manifest or tar bundle representing a previous delivery. |
| `--help`                       | `-h`       |                                  | Show this message and exit.                                      |

### `hopctl generate`

`Usage: hopctl generate [OPTIONS] COMMAND [ARGS]...`

Generate in-toto keys/layout for Hoppr input files

#### `hopctl generate layout`

`Usage: hopctl generate layout [OPTIONS]`

Generate in-toto layout based on transfer file.

##### Options

| Name(s)                        | Shorthand  | Env Var                          | Description                                             |
| ------------------------------ | ---------- | -------------------------------- | ------------------------------------------------------- |
| `--transfer`                   | `-t`       | HOPPR_TRANSFER_CONFIG            | Specify transfer config. (default: transfer.yml)        |
| `--project-owner-key`          | `-pk`      | HOPPR_PROJECT_OWNER_KEY          | Path to key used to sign in-toto layout. (**required**) |
| `--functionary-key`            | `-fk`      | HOPPR_FUNCTIONARY_KEY            | Path to key used to sign in-toto layout. (**required**) |
| `--prompt`                     | `-p`       | HOPPR_PROJECT_OWNER_KEY_PROMPT   | Prompt user for project owner key`s password.           |
| `--project-owner-key-password` | `-pk-pass` | HOPPR_PROJECT_OWNER_KEY_PASSWORD | Password for project owner key.                         |
| `--help`                       | `-h`       |                                  | Show this message and exit.                             |

### `hopctl merge`

`Usage: hopctl merge [OPTIONS]`

Merge all properties of two or more SBOM files.

##### Options

| Name(s)         | Shorthand | Type      | Description                                                                 |
| --------------- | --------- | --------- | --------------------------------------------------------------------------- |
| `--manifest`    | `-m`      | File      | Manifest file containing SBOMs to merge.                                    |
| `--sbom`        | `-s`      | File      | SBOM file to merge (can be specified multiple times).                       |
| `--sbom-dir`    | `-d`      | Directory | Directory containing SBOM files to merge (can be specified multiple times). |
| `--sbom-url`    | `-u`      | URL       | URL of SBOM to merge (can be specified multiple times).                     |
| `--output-file` | `-o`      | File      | Path to output file [default: hopctl-merge-YYMMDD-HHMMSS.json].             |
| `--deep-merge`  |           |           | Resolve and expand `externalReferences` in-place.                           |
| `--flatten`     |           |           | Flatten nested `components` into single unified list.                       |
| `--help`        | `-h`      |           | Show this message and exit.                                                 |

### `hopctl validate`

`Usage: hopctl validate [OPTIONS] INPUT_FILES...`

Validate multiple manifest files for schema errors.

##### Arguments

| Name        | Description              |
| ----------- | ------------------------ |
| input_files | Paths to manifest files. |

##### Options

| Name(s)         | Shorthand | Type | Env Var               | Description                                      |
| --------------- | --------- | ---- | --------------------- | ------------------------------------------------ |
| `--credentials` | `-c`      | Path | HOPPR_CREDS_CONFIG    | Specify credentials config for services.         |
| `--transfer`    | `-t`      | Path | HOPPR_TRANSFER_CONFIG | Specify transfer config. (default: transfer.yml) |
| `--help`        | `-h`      |      |                       | Show this message and exit.                      |

### `hopctl version`

Print version information for hoppr.
