---
sidebar_position: 1
title: What is Droppr?
description: Unpacks Hoppr Bundles
---

# What Is Droppr?

**Droppr** is a command-line tool to facilitate the unpacking of bundles created by [Hoppr](https://hoppr.dev).

<div style={{ textAlign: 'center' }}>
  <img style={{ maxHeight: '400px'}} src="/img/droppr-img/DropprDragon-01.png" />
</div>


Droppr is written in the "Go" language, to produce a single executable, simplifying installation and usage.

## Artifact Delivery Options
Artifacts from the Hoppr bundle can be delivered
- to repositories in a [Nexus](https://www.sonatype.com/products/sonatype-nexus-repository) instance
- as files on the local file system
- as a direct installation on the target system (where applicable)

The delivery location is defined in the config file, and may vary between repos/purl-types.

## PURL Types

Dropper supports the following PURL types:
- `Docker`
- `Generic`
- `Git`
- `Helm`
- `Maven`
- `Pypi`
- `RPM`
- `APT`
- `NPM`
- `Nuget`

## Limitations

Droppr currently only supports TAR bundles (`tar` and `tar.gz`) from Hoppr, and only those built with Hoppr version 1.8.0 and later.


## Droppr Repository
[Droppr GitLab](https://gitlab.com/hoppr/droppr)

## License

Droppr is [MIT Licensed](https://gitlab.com/hoppr/droppr/-/raw/main/LICENSE).
