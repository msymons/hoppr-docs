---
sidebar_position: 3
title: Droppr Changelog
description: Droppr Changelog 
---

# Change Log

For more information about releases, see the Droppr [Change Log](https://gitlab.com/hoppr/droppr/-/blob/main/docs/CHANGELOG.md)