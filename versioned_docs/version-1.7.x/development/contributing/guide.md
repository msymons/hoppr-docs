---
sidebar_position: 1
---

# Contribution Guide

:::tip

Read the 3 contribution guide sections -  **Etiquette, Viability, and Process** - before creating an issue or merge request.

:::

## 1. Etiquette

- Please read and follow the [Code of Conduct](./code_of_conduct.md)
- Please be considerate towards the developers and other users when raising issues or presenting merge requests
- Respect our decision(s), and do not be upset or abusive if your submission is not used
- Most of all, **have fun!**

## 2. Viability

When requesting or submitting new features, first consider whether it might be useful to others. Open source
projects are used by many people, who may have entirely different needs than your own. Think about whether or not
your feature is likely to be used by other users of the project. The project maintainer may close an issue
or merge request if it does not align with the project vision.

## 3. Process

### Submitting an issue

When contributing to Hoppr, please __first discuss__ the change you wish to make via a GitLab issue:

- [Bug](https://gitlab.com/lmco/hoppr/hoppr/-/issues/new?issuable_template=Bug)
- [Feature Request](https://gitlab.com/lmco/hoppr/hoppr/-/issues/new?issuable_template=Feature%20Request)

### Before submitting a merge request

1. Wait until a maintainer agrees to a MR, which is indicated with a [Contrib::Accepting MRs](https://gitlab.com/groups/hoppr/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Contrib%3A%3AAccepting%20MRs&first_page_size=20) label
1. Read and understand the [DCO guidelines](developer_certificate_of_origin.md) for the project
1. [Create a fork](https://gitlab.com/hoppr/hoppr/-/forks/new) of Hoppr
1. Follow the [development documentation](../extending/plugins/create-a-plugin.mdx) to make changes in your fork


### Technical Requirements

- Use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) to document what changed
- Git committer email must match your [verified emails](https://gitlab.com/-/profile/emails)
- Only [signed commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html) can be 
  pushed to this repository
- All committers must agree with the [Developer Certificate of Origin](developer_certificate_of_origin.md) using the
   `--signoff` option
- The combined git commit command for signing and signoff is:
  - `git commit -Ssam "fix: Hoppr patch contribution"`

### Hints

#### Conventional Commits

Please use [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) to indicate
what has changed in merge requests.

Examples:

- `fix(docs): Corrected package urls supported`
- `feat(plugin): Implemented new required argument to plugin interfaces`

#### GPG Commit Signing

The [GitLab documentation](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html) has an excellent walk through of signing commits.  While you can sign individual commits, you might consider setting the global config to sign commits automatically:

- `git config --global commit.gpgsign true`