---
title: What Is an SBOM?
description: This page describes what a Software Bill of Materials is and what is needed/required to create and share.
sidebar_position: 1
slug: /what-is-an-sbom
---

A Software Bill of Materials (SBOM) is a list of all components (both open source and third-party) in a
software release. This information is used for security analysis, license compliance, and configuration management.

NTIA has released a several part video explaining what is an SBOM:

- [SBOM Explainer: What Is an SBOM? Part 1](https://www.youtube.com/watch?v=6yljBKKl8Vo&list=PLO2lqCK7WyTDpVmcHsy6R2HWftFkUp6zG&index=2)
- [SBOM Explainer: What is an SBOM? Part 2](https://www.youtube.com/watch?v=7IJaNdFwwBo&list=PLO2lqCK7WyTDpVmcHsy6R2HWftFkUp6zG&index=3)
- [SBOM Explainer: What is an SBOM? Part 3](https://www.youtube.com/watch?v=1_GpbwR9f94&list=PLO2lqCK7WyTDpVmcHsy6R2HWftFkUp6zG&index=4)

## Why are SBOMs a Necessity?

[White House Executive Order 14028](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/)
mandated changes in the development and procurement of software by the US Federal Government to address persistent
and increasingly sophisticated malicious cyber campaigns that threaten the public sector. This mandate resulted
in responses from National Institute of Standards and Technology (NIST), National Telecommunications and Information
Administration (NTIA), and Office of Management and Budget (OMB).

[OMB M-22-18](https://www.whitehouse.gov/wp-content/uploads/2022/09/M-22-18.pdf) requires contracts to include
provisions to allow delivery of SBOMs as a part of demonstrating conformance to secure software development practices.
These SBOMs must meet [NTIA's The Minimum Elements For a Software Bill of Materials (SBOM)](https://www.ntia.doc.gov/files/ntia/publications/sbom_minimum_elements_report.pdf).

## What is in an SBOM?

The [NTIA's The Minimum Elements For a Software Bill of Materials (SBOM)](https://www.ntia.doc.gov/files/ntia/publications/sbom_minimum_elements_report.pdf) outlines minimum elements of an SBOM to meet [White House Executive Order
14028](https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/) mandates.

- Supplier Name
- Component Name
- Version of the Component
- Other Unique Identifiers
- Dependency Relationship
- Author of SBOM Data
- Timestamp

Additionally license information should be included in the SBOM for each component to ensure
license compliance can be automated.

- License Information

## Which SBOM Standard to Use?

We require using [CycloneDX](https://cyclonedx.org/) as an industry open standard developed by the Open
Web Application Security Project (OWASP) community. It is a full-stack SBOM format focused on ease of
adoption and automation of SBOM generation throughout your software development pipeline.

A minimum version of [CycloneDX 1.4](https://cyclonedx.org/docs/1.4/json/) with [CycloneDX 1.5](https://cyclonedx.org/docs/1.5/json/)
or later preferred because of the support for [commercial license information](https://cyclonedx.org/docs/1.5/json/#components_items_licenses_oneOf_i0_items_license).

We do not support other standards like SPDX. If you generate SPDX SBOMs, please use a converstion tool to CycloneDX.
Your mileage may vary on the effectiveness of conversion tools.

## How to create an SBOM?

The best time to generate an SBOM is during the software build, as this is the time in which dependences are known
and easily analyzed. CycloneDX maintains [cdxgen](https://github.com/CycloneDX/cdxgen) which supports a variety of
language package managers. If build-time SBOM generation is not available, tools like
[Trivy](https://aquasecurity.github.io/trivy/) and [Syft](https://github.com/anchore/syft) can create CycloneDX
SBOMs through analysis.

Ultimately the CycloneDX SBOMs may need the NTIA minimum elements added. We recommend using common linux tools like
`jq` to add missing infomation in a automated fashion.

## How to Validate an SBOM?

We encourage using [Hoppr](/docs/category/using-hoppr) to validate SBOMs before submitting to customers
to ensure compliance to the required elements.
