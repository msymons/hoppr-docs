---
sidebar_position: 1
---

# Hoppr Processing 101

Hoppr has a simple architecture. Prepare a few core input files for the Hippo, run `hopctl`, and receive outputs. Details below:

<img style={{ borderRadius : '7px'}} src="/img/hoppr-components/hoppr-processing.jpg" />

###  Hoppr Processing

- [Core Plugins](../core-plugins/core-plugins.mdx)
- [Ecosystem Plugins](../ecosystem-plugins/ecosystem-plugins.mdx)

:::note

See more architectural processing details on our [execution sequence diagrams](../../architecture/sequence-diagrams.md).

:::

### Four Input Files

1. **[CycloneDX SBOMs (JSON)](../input-files/sbom.md)**
    - A list of [software components](https://cyclonedx.org/specification/overview/#components) (e.g. software packages, digital assets)
1. **[Manifest (YAML)](../input-files/manifest.md)**
    - A composite of all **CycloneDX SBOMs** to be processed; may include other **Manifests**
    - Identifies repositories from which software components are to be retrieved
1. **[Transfer (YAML)](../input-files/transfer.md)**
    - A list of stages for Hoppr to execute; each stage is a list of Hoppr processing plugins.
1. **[Credentials (YAML)](../input-files/credentials.md)**
    - _(Optional)_ - specifies authentication credentials for Hoppr network activity

## Input File Schemas

- [Manifest v1](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-manifest-schema-v1.json)
- [Transfers v1](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-transfers-schema-v1.json)
- [Credentials v1](https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-credentials-schema-v1.json)
- [CycloneDX SBOMs](https://github.com/CycloneDX/specification/tree/master/schema)

### Schemas

All input file schemas are in JSON as YAML can be converted to JSON and validated against the schema.  Example schema validation using [yq](https://mikefarah.gitbook.io/yq) and [jsonschema](https://pypi.org/project/jsonschema/).

```bash
$ cat airgapped.yml | yq eval -P -o json > airgap-manifest.json
$ curl "https://gitlab.com/api/v4/projects/34748703/packages/generic/schemas/v1/hoppr-manifest-schema-v1.json" \
  -o manifest-schema.json

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  100  3205  100  3205    0     0    436      0  0:00:07  0:00:07 --:--:--   740

$ jsonschema --instance airgap-manifest.json manifest-schema.json
$ echo $?
0
```

### Outputs

- Bundles - [TAR](./your-first-bundle.md) and [Nexus](./nexus-bundle.md)
- Reports
- [Anything you can build!](../../development/extending/plugins/create-a-plugin.mdx)

## Example Hoppr Project

This is a simplified example to illustrate the input files and the relationship between them.

```mermaid
graph LR

subgraph Product B
    manifest-b.yml --> sbom-b1.json
    manifest-b.yml --> sbom-b2.json
end

subgraph Product A
    manifest-a.yml --> sbom-a.json
end

subgraph Hoppr Input Files
    manifest.yml --> manifest-a.yml
    manifest.yml --> manifest-b.yml
    transfer.yml
    credentials.yml
end
```

In this example, the root `manifest` references manifests for two other products.

### Example SBOMs & Manifests

- **Product A** contains a CycloneDX `SBOM` on the "as-built" components that need to be transferred and their build dependencies.
- **Product B** contains two CycloneDX `SBOMs` for two different components that need to be transferred and their build dependencies.
- Both Products have `Manifest` files to specify what `SBOMs` are needed for the product to work. Each manifest also specifies a list of repositories to be searched for components specified in their `SBOMs`.
- Lastly, the third party has a `Manifest` that has either local or URL includes of **Product A** and **Product B**'s Manifests, but does not include any `SBOMs` directly.

We recommend using a tool like [renovate](https://github.com/renovatebot/renovate) to keep your source projects up-to-date, and include the generation of `Manifest` and `SBOM` files in your continuous delivery pipeline(s).  In this way, any transfers that are made with **Hoppr** can be kept current as well.

### Example Transfer

For more information on configuration options see the transfer file [documentation](../input-files/transfer.md)

### Example Credentials

For more information on configuration options see the credentials file [documentation](../input-files/credentials.md)
