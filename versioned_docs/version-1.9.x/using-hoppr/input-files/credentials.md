---
sidebar_position: 1
title: Credential
---

## Credential

The `Credentials` file specifies any credentials needed to access the `Manifest`, `SBOMs`, or `Components` that are access controlled.

A **Hoppr** specific YAML file that defines how to find credentials to access [Manifest](https://hoppr.dev/docs/using-hoppr/input-files/manifest.md), [SBOMs](https://hoppr.dev/docs/using-hoppr/tutorials/processing-101/#cyclonedx-sboms), and the components that will be collected.

In the example below, for `user_env` and `pass_env`: when the `_env` suffix is used, it will read it as an environment variable.

```yaml
---
schemaVersion: v1
kind: authentication

metadata:
  name: Example Credentials File
  version: "1.0.0" 
  description: This is an example credentials.yml file 

credential_required_services:
   - url: https://hoppr.dev/ # Protocol is not required i.e hoppr.dev/

     user: UserName
     .. or ...
     user_env: USER_ENV

     pass_env: USER_TOKEN
```

For more information on transfer configuration options see the transfer file [documentation](../input-files/transfer.md)
For more information on manifest configuration options see the manifest file [documentation](../input-files/manifest.md)
