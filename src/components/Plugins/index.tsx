import React, { useState } from 'react';
import CodeBlock from '@theme/CodeBlock';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { PluginList, DependencyMap } from './data';

function Purl({name}) : JSX.Element {
    return (
        <code className="padding--sm margin-right--sm">{name}</code>
    );
  }

function Dep({name}) : JSX.Element {
    var site = DependencyMap.find(d => d.name.toLowerCase() === name.toLowerCase()).site;
    return (site === "") ? 
    (
        <code className="padding--sm margin-right--sm">{name}</code>
    ) :
    (
        <a href={site} target="_blank" rel="nofollow"><code className="padding--sm margin-right--sm">{name}</code></a>
    );
  }

function CopyWrapper({transferTag}) {
  const [copied, setCopied] = useState(false);
  return (
    <CopyToClipboard text={transferTag} onCopy={() => setCopied(true)}>
        <button className="button button--outline button--secondary margin-left--sm">{copied ? `✔️ Copied` : `📋 Copy`}</button>
    </CopyToClipboard>
  );
}

function TransferExample({plugin, config}) : JSX.Element {
    return (
        <CodeBlock language="yaml" showLineNumbers>
{
`---
schemaVersion: v1
kind: transfer

stages:
  ${plugin.pluginType}:
    plugins:
    - name: "${plugin.tag}"
      config:
      ${config}
max_processes: 3`
}
        </CodeBlock>
    );
}


export default function Plugin({children, transferTag, showDefaultExample = true}) : JSX.Element {
    var plugin = PluginList.find(p => p.tag.toLowerCase() === transferTag.toLowerCase());
    return (
      <div style={{margin: '2rem 0 0 0'}}>
          <h2>Details</h2>
          <div className="card">
              <div className="card__body">
              <div className="padding-left--md">
                  <h4 className="padding-top--md">Transfer Tag</h4>
                  <div className="padding-left--md padding-bottom--md">
                      <code className="padding--sm">{transferTag}</code>
                      <CopyWrapper transferTag={transferTag} />
                  </div>
                  <h4>Supported PURLs</h4>
                  <div className="padding-left--md padding-bottom--md">
                      { plugin.supportedPurls.length > 0 ? 
                            plugin.supportedPurls.map(purl => <Purl name={purl} />) :
                            <code className="padding--sm margin-right--sm">ALL</code>
                       }
                  </div>
                  <h4>System Dependencies</h4>
                  <div className="padding-left--md padding-bottom--md">
                      {plugin.dependencies.length > 0 ? 
                        plugin.dependencies.map(dep => <Dep name={dep} />) :
                        <code className="padding--sm margin-right--sm">NONE</code>
                      }
                  </div>
              </div>
              </div>
          </div>
          { showDefaultExample && children &&
            <div className="padding-top--md">
                <h2>Transfer File Example</h2>
                <TransferExample plugin={plugin} config={children} />
            </div>
          }
      </div>
    );
  }